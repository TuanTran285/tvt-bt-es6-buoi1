// lấy ra được dòng chữ
const heading = document.querySelector(".heading").innerText

const hoverMe = () => {
    const value = heading.slice().split('').map(item => {
        return `<span>${item}</span>`
    })
    document.querySelector(".heading").innerHTML = value.join('')
}
hoverMe()