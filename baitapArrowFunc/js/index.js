const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermil", "lion", "lavender","celadon","saffron","fuschia","cinnabar"];

const renderColorList = () => {
    const htmls = colorList.map(item => {
        return `<button class="color-button ${item}"></button>`
    })
    document.getElementById("colorContainer").innerHTML = htmls.join('')
}


const handleEvents = () => {
    const buttons = document.querySelectorAll(".color-button")
    Array.from(buttons).forEach((item, index) => {
        item.addEventListener("click", () => {
            Array.from(buttons).forEach(b => {
                b.classList.remove("active")
            })
            item.classList.add("active")
            colorList.forEach(color => {
            document.querySelector(".house").classList.remove(color)
            })
            document.querySelector(".house").classList.add(colorList[index])
        })
        
    })
}

const start = () => {
    renderColorList()
    handleEvents()
}


start()