const tinhDTB = (...params) => {
  const length = params.length;
  return params.reduce((acc, currValue) => {
    return acc + (currValue * 1) / length;
  }, 0);
};

document.getElementById("btnKhoi1").addEventListener("click", () => {
  const toan = document.getElementById("inpToan").value;
  const ly = document.getElementById("inpLy").value;
  const hoa = document.getElementById("inpHoa").value;
  if(toan=="" || ly=="" || hoa == "") return
  const result = tinhDTB(toan, ly, hoa);
  document.getElementById("tbKhoi1").innerHTML = result.toFixed(2);
});

document.getElementById("btnKhoi2").addEventListener("click", () => {
  const van = document.getElementById("inpVan").value;
  const su = document.getElementById("inpSu").value;
  const dia = document.getElementById("inpDia").value;
  const anhVan = document.getElementById("inpEnglish").value;
  if(van == "" || su == "" || dia == "" || anhVan == "") return
  const result = tinhDTB(van, su, dia, anhVan)
  document.getElementById("tbKhoi2").innerHTML = result.toFixed(2)
});
